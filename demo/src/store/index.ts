import {legacy_createStore,applyMiddleware,combineReducers} from 'redux'

import logger from 'redux-logger'

import thunk from 'redux-thunk'

import reducer from './modlues/reduce'

const store1=combineReducers({
    reducer
})

export default legacy_createStore(store1,applyMiddleware(thunk,logger))