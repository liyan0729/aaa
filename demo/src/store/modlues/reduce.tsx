const initialState = {}

export default (state = initialState, { type, payload }:any) => {
    const newstate=JSON.parse(JSON.stringify(state))
  switch (type) {

  case "first":
    return { ...state, ...payload }

  default:
    return state
  }
}
