import React from 'react'
import { Outlet, NavLink } from 'react-router-dom'
import { Tabbar } from 'react-vant'
import { FriendsO, HomeO, Search, SettingO } from '@react-vant/icons'
type Props = {}

const Home = (props: Props) => {
    return (
        <div className='home'>
            <main>
                <Outlet />
            </main>
            <footer>
                <Tabbar>
                    <Tabbar.Item icon={<HomeO />}><NavLink to='/home/Shou'>标签</NavLink></Tabbar.Item>
                    <Tabbar.Item icon={<Search />}><NavLink to='/home/Fen'>标签</NavLink></Tabbar.Item>
                    <Tabbar.Item icon={<FriendsO />}><NavLink to='/home/Che'>标签</NavLink></Tabbar.Item>
                    <Tabbar.Item icon={<SettingO />}><NavLink to='/home/My'>标签</NavLink></Tabbar.Item>
                </Tabbar>
            </footer>
        </div>
    )
}

export default Home