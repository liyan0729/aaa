import { lazy } from 'react'
import { routerlist } from '../utils/types.d'
const router: routerlist = [
    {
        path: "/",
        redirect: "/home/Shou"
    },
    {
        path: "/home",
        component: lazy(() => import("../views/Home")),
        children: [
            {
                path: "/home/Shou",
                component: lazy(() => import("../views/home/Shou")),
            },
            {
                path: "/home/Fen",
                component: lazy(() => import("../views/home/Fen")),
            },
            {
                path: "/home/Che",
                component: lazy(() => import("../views/home/Che")),
            },
            {
                path: "/home/My",
                component: lazy(() => import("../views/home/My")),
            }
        ]
    }

]

export default router