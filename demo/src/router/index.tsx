import React, { Suspense } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import { routeritem, routerlist } from '../utils/types.d'
import router from './routerconfig'
type Props = {}

const index = (props: Props) => {
  const renderrouter = (router: routerlist) => {
    return router.map((item: routeritem, index: number) => {
      return <Route
        key={index}
        path={item.path}
        element={
          item.redirect ? <Navigate to={item.redirect}></Navigate> : <item.component />
        }
      >
        {
          item.children && renderrouter(item.children)
        }
      </Route>
    })


  }
  return (
    <Suspense>
      <BrowserRouter>
        <Routes>
          {
            renderrouter(router)
          }
        </Routes>
      </BrowserRouter>
    </Suspense>
  )
}

export default index
