import store from "../store"




export type routerlist = Array<routeritem>

export type routeritem={
    path:string,
    redirect?:string,
    component?:any,
    children?:routerlist
}


export type rootdispacth=typeof store.dispatch


